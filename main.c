#include "interaction.h"
#include "affichage.h"
#include "raycasting.h"
#include "common.h"
#include "kruskal.h"


int main(void) {
	

	// Déclaration de notre tableau:
	int tab[tab_SIZE][tab_SIZE];
	srand(time(NULL));

	// Initialisation des contours:
	init_tableau(tab);

	// Génération aléatoire des murs:
	Kruskal(tab);
	replace_1_en_0(tab);
	depart_arrive(tab);
	affiche_tab(tab);
	printf("\n");

    // La génération aléatoire mise dans la map
	worldmap map;
	map_crea(map,tab);
	
	// Initialisaiton des paramètres de départ
	float x = 100, y = 100;
	position pos = {x,y};
	float vx = 1, vy = 0.0;
	
	// Premier affichage
	raycasting_display(pos, vx, vy, map);
	update_screen();
	
	// Initialisation de variable pour la boucle
	int continuer = 1;
	int compt = 0;
	int lancerSon = 1;
	
	SDL_Event event;

	//Création et initialisation d'un objet système pour un son court
	FMOD_SYSTEM *system;
	FMOD_System_Create(&system);
	FMOD_System_Init(system,4,FMOD_INIT_NORMAL,NULL);
	FMOD_SOUND *pas = NULL;
	FMOD_SOUND *backgroundmusic = NULL;
	FMOD_SOUND *victory_sound = NULL;
	FMOD_SOUND *collision = NULL;
	FMOD_System_CreateSound(system,"sounds/footstep.wav",FMOD_CREATESAMPLE,0,&pas);
	FMOD_System_CreateSound(system,"sounds/victory_sound.wav",FMOD_CREATESAMPLE,0,&victory_sound);
	FMOD_System_CreateSound(system,"sounds/collision.wav",FMOD_CREATESAMPLE,0,&collision);
	FMOD_System_CreateSound(system,"sounds/backgroundmusic.wav",FMOD_CREATESAMPLE| FMOD_LOOP_NORMAL,0,&backgroundmusic);
	
	//Répétition de la musique de fond
	FMOD_Sound_SetLoopCount(backgroundmusic,-1);
	//Lancement de la musique d'ambiance
	FMOD_System_PlaySound(system,backgroundmusic,NULL,0,NULL);

    /* autorise la répétition des touches à une cadence de 10 ms */
	SDL_EnableKeyRepeat(10,20);
	
	/* boucle évenementielle */
	while(continuer) {
		SDL_WaitEvent(&event);
		switch(event.type) {
			case SDL_KEYDOWN: // touche enfoncée
				switch(event.key.keysym.sym) {
					case SDLK_ESCAPE: continuer = 0; break;
					case SDLK_UP: // touche haut
					    //Son lancé en fonction du compteur
						if(lancerSon){FMOD_System_PlaySound(system,pas,NULL,0,NULL);}
						//Test et lancement de son pour collision
						position posBefore = pos;
						continuer = arrow_up(&pos, vx, vy, map);
						if(posBefore.x == pos.x && posBefore.y == pos.y){
							int j = 50000000;
							FMOD_System_PlaySound(system,collision,NULL,0,NULL);
							while(j!=0){j-=1;}
						}
						//Test et lancement de son pour la victoire
						if(continuer==0){
							int i=600000000;
							FMOD_System_PlaySound(system,victory_sound,NULL,0,NULL);
							while(i!=0){i-=1;}
						}
						break;
					case SDLK_DOWN:
						if(lancerSon){FMOD_System_PlaySound(system,pas,NULL,0,NULL);}
						continuer = arrow_down(&pos, vx, vy, map);
						if(continuer==0){
							int i=600000000;
							FMOD_System_PlaySound(system,victory_sound,NULL,0,NULL);
							while(i!=0){i-=1;}
						}
						break;
					case SDLK_RIGHT:
						arrow_right(&vx, &vy);
						break;
					case SDLK_LEFT:
						arrow_left(&vx, &vy);
						break;
					default:
						break;
				}
				break;
			case SDL_QUIT: // si on ferme manuellement en mode fenêtre
				continuer = 0;
				break;
		}
		//Compteur permettant d'avoir une meilleure gestion du son
		if(compt<=10){
			compt+=1;
			lancerSon=0;
		}
		else{
			compt=0;
			lancerSon=1;
		}
		raycasting_display(pos, vx, vy, map);
		update_screen();
	}
	//Désallocation du son
	FMOD_Sound_Release(pas);
	FMOD_Sound_Release(victory_sound);
	FMOD_Sound_Release(collision);
	FMOD_Sound_Release(backgroundmusic);
	FMOD_System_Close(system);
	FMOD_System_Release(system);
	return 0;
}
