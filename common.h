/** @file */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __COMMON_H
#define __COMMON_H

/** The worldmap width in number of blocks. */
#define MAP_WIDTH 9
/** The worldmap heigth in number of blocks. */
#define MAP_HEIGHT MAP_WIDTH

/** The lenght of the block edges in real-world units. */
#define WALL_SIZE 64
/** The height of the FPV camera in real-world units. */
#define CAMERA_HEIGHT 42
/** The distance from the FPV camera to the screen in real-world units. */
#define DISTANCE_SCREEN_CAMERA 320

/** The width of the screen. */
#define SCREEN_WIDTH 500
/** The height of the screen. */
#define SCREEN_HEIGHT 500

/** The color index of the winning block. */
#define WINNING_COLOR 5
/** The upper limit in the number of frames per second. */
#define MAX_FRAMERATE 25.0

/** Type definition for pixel color indexes. */
typedef char color;
/** Type definition for worldmaps. */
typedef color worldmap[MAP_WIDTH][MAP_HEIGHT];
/** Type definition for position structures. */
typedef struct position {
    float x;
    float y;
} position;
/** Type definition for a structure storing information on distant walls. */
typedef struct dist_color_and_angle {
    float distance;
    color col;
    float angle;
} dist_color_and_angle;

#endif /* __COMMON_H */
