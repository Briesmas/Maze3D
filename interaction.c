/**
  ******************************************************************************
  * @file    interaction.c
  * @author  Matthieu Guerquin-Kern
  * @version V0.1
  * @date    06-July-2016
  * @brief   This file includes the functions to move in a maze defined
  * 			by a 2D block-based map. Functions are target agnostic.
  ******************************************************************************
 **/

/* Includes ------------------------------------------------------------------*/
#include "interaction.h"

/**
  * @brief  Move forward (if using a keyboard, corresponds to pressing
  * 		arrow up). Checks if game is ended and prevents to go
  * 		through walls.
  * @param  pos	position
  * @param	vx	x-component of direction
  * @param	vy	y-component of direction
  * @param	map	worldmap
  * @return winning flag
  * @retval 1	if winning block is reached,
  * @retval	0	otherwise
  */

int arrow_up(position *pos, float vx, float vy, worldmap map) {
    float x_new = pos->x + vx * SPEED;
    float y_new = pos->y + vy * SPEED;
    int ni = floor(x_new/WALL_SIZE);
    int nj = floor(y_new/WALL_SIZE);
    color next_location = map[nj][ni];

	if (next_location==WINNING_COLOR){
		printf("Victoire\n");
		return 0;  //end game
	}
	if (next_location==0){
	    pos->x = x_new;
		pos->y = y_new;
	}
	return 1;
}

/**
  * @brief  Move backward (if using a keyboard, might be called when arrow down
  * 			is pressed).
  * @param  pos	position
  * @param	vx	x-component of direction
  * @param	vy	y-component of direction
  */
int arrow_down(position *pos, float vx, float vy, worldmap map) {
    float x_new = pos->x - vx * SPEED;
    float y_new = pos->y - vy * SPEED;
    int ni = floor(x_new/WALL_SIZE);
    int nj = floor(y_new/WALL_SIZE);
    color next_location = map[nj][ni];
	if (next_location==WINNING_COLOR){
		printf("Victoire\n");
		return 0;  //end game
	}
	else if (next_location==0){
	    pos->x = x_new;
		pos->y = y_new;
	}
	return 1;
}

/**
  * @brief  rotation of the direction vector
  * @param	vx		x-component of direction
  * @param	vy		y-component of direction
  * @param  theta	angle increment
  */
static void rotation(float *vx, float *vy, float theta) {
    *vx = *vx * cos(theta) - *vy * sin(theta);
    *vy = *vx * sin(theta) + *vy * cos(theta);
    float norm = sqrt(*vx * *vx + *vy * *vy);
    *vx /= norm;
    *vy /= norm;
}

/**
  * @brief  Rotate right
  * @param	vx	x-component of direction
  * @param	vy	y-component of direction
  */
void arrow_right(float *vx, float *vy) {
    rotation(vx, vy, ANGLE_INCREMENT);
}
/**
  * @brief  Rotate left
  * @param	vx	x-component of direction
  * @param	vy	y-component of direction
  */
void arrow_left(float *vx, float *vy) {
    rotation(vx, vy, -ANGLE_INCREMENT);
}
