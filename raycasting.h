/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __RAYCASTING_H
#define __RAYCASTING_H

#ifdef __cplusplus
 extern "C" {
#endif

#include <math.h>
#include "common.h"

dist_color_and_angle dda( position, float , float , worldmap );
void raycasting_display( position, float, float, worldmap );

#ifdef __cplusplus
}
#endif

#endif /* __RAYCASTING_H */
