/**
  ******************************************************************************
  * @file    raycasting.c
  * @author  Matthieu Guerquin-Kern
  * @version V0.1
  * @date    02-November-2016
  * @brief   This file includes the functions to render a 2D block-based
  * 			map in 3D using the raycasting technique. Functions are
  * 			target agnostic.
  ******************************************************************************
 **/

/* Includes ------------------------------------------------------------------*/
#include "raycasting.h"
#include "affichage.h"

/**
  * @brief  Returns the color of the first block encountered horizontally
  * @param  xprime	x coordinate
  * @param	yprime	y coordinate
  * @param	vy		y-component of direction
  * @param	map		worldmap
  * @return color of the first block encountered horizontally
  */
static color tangent_wall_h(float xprime, int yprime, float vy, worldmap map){
    int i=(int) (xprime/WALL_SIZE), j=yprime/WALL_SIZE;
    if (vy<0){
        j--;
    }
    if ((i<0)||(j<0)||(i>=MAP_WIDTH)||(j>=MAP_HEIGHT)){
        /* sortie de la carte */
        return 3; /* couleur du bord de carte */
    }else{
        return map[j][i];
    }
}

/**
  * @brief  Returns the color of the first block encountered vertically
  * @param  xprime	x coordinate
  * @param	yprime	y coordinate
  * @param	vx		x-component of direction
  * @param	map		worldmap
  * @return color of the first block encountered vertically
  */
static color tangent_wall_v(int xprime, float yprime, float vx, worldmap map) {
    int j = xprime / WALL_SIZE;
    int i = (int) (yprime / WALL_SIZE);
    if (vx < 0) {
        j--;
    }
    
    /* Attention, si l'on sort du plateau, retourner 3 */
    if (i < 0 || i >= MAP_WIDTH || j < 0 || j >= MAP_HEIGHT) {
        return 3;
    } else {
        return map[i][j];
    }
}

/**
  * @brief  Returns the color, distance and angle of the next non-empty
  * 			block encountered horizontally
  * @param  pos		position
  * @param	vx		x-component of direction
  * @param	vy		y-component of direction
  * @param	map		worldmap
  * @return color, distance and angle of the first block encountered horizontally
  */
static dist_color_and_angle dda_h(position pos, float vx, float vy, worldmap map) {
    dist_color_and_angle dc;
    int j=pos.y/WALL_SIZE,dj=1;
    if (vy>0){
        j++;
    }else{
        dj=-1;
    }
    float dy=fabs(WALL_SIZE*j-pos.y); /* distance verticale au premier bord
    horizontal dans la direction d'observation (en pixels) */
    float d1; /* distance au premier bord horizontal dans la direction d'observation (en pixels) */
    float Dx = vx/fabs(vy); /* distance horizontale (en cases)
    parcourue quand une case est parcourue verticalement */
    float du=WALL_SIZE*sqrt(1+Dx*Dx); /* increment de distance lors du passage
    au bord horizontal suivant (en pixels) */
    if (vy==0){ /* dans le cas dégénéré où la direction d'observation
        est pile horizontale, on évite éventuellement la division par 0 */
        dc.distance = INFINITY;
        dc.col=tangent_wall_h( pos.x, pos.y, vy, map);
        return dc;
    }
    pos.x += dy*Dx; /* first horizontal line crossing coordinate */
    d1 = dy*du/WALL_SIZE;   
    dc.distance = d1;
    while ((dc.col=tangent_wall_h( pos.x, WALL_SIZE*j, vy, map))==0){
        j += dj;
        pos.x += WALL_SIZE*Dx;
        dc.distance += du;
    }
    dc.angle = 90.0-180.0*(vy<0);
    return dc;  
}

/**
  * @brief  Returns the color, distance and angle of the next non-empty
  * 			block encountered vertically
  * @param  pos		position
  * @param	vx		x-component of direction
  * @param	vy		y-component of direction
  * @param	map		worldmap
  * @return color, distance and angle of the first block encountered
  * 			vertically
  */
static dist_color_and_angle dda_v(position pos, float vx, float vy, worldmap map) {
    dist_color_and_angle dc;
    int i=pos.x/WALL_SIZE,di=1;
    if (vx>0){
        i++;
    }else{
        di=-1;
    }
    float dx=fabs(WALL_SIZE*i-pos.x);  /* distance horizontale au premier bord
    vertical dans la direction d'observation (en pixels) */
    float d1;  /* distance au premier bord vertical dans la direction d'observation (en pixels) */
    float Dy=vy/fabs(vx); /* distance verticale (en cases)
    parcourue quand une case est parcourue horizontalement */
    float du=WALL_SIZE*sqrt(1+Dy*Dy); /* increment de distance lors du passage
    au bord vertical suivant (en pixels) */
    if (vx==0){ /* dans le cas dégénéré où la direction d'observation
        est pile verticale */
        dc.distance = INFINITY;
        dc.col=tangent_wall_v( pos.x, pos.y, vx, map);
        return dc;
    }
    pos.y += dx*Dy; /* first vertical line crossing coordinate */
    d1 = dx*du/WALL_SIZE;
    dc.distance = d1;
    /*printf("init DDA: vx= %.5f, vy= %.2f, y= %.2f, dx= %.5f, dy= %.2f, d=%.1f, dist=%.1f\n",vx,vy,y/64,dx,dy,du,dc.distance);*/
    while ((dc.col=tangent_wall_v( WALL_SIZE*i, pos.y, vx, map))==0){
        i += di;
        pos.y += WALL_SIZE*Dy;
        dc.distance += du;
    }
    dc.angle = 180.0*(vx<0);
    return dc;  
}

/**
  * @brief  Returns the color, distance and angle of the next non-empty
  * 			block encountered
  * @param  pos		position
  * @param	vx		x-component of direction
  * @param	vy		y-component of direction
  * @param	map		worldmap
  * @return color, distance and angle of the first block encountered
  * 	horizontally
  */
dist_color_and_angle dda(position pos, float vx, float vy, worldmap map) {
    dist_color_and_angle dch = dda_h(pos, vx, vy, map),dcv = dda_v(pos, vx, vy, map);
    if (dch.distance<dcv.distance){
        return dch;
    }
    return dcv; 
}

/**
  * @brief  Renders a new frame using the raycasting method
  * @param  pos		position
  * @param	vx		x-component of direction
  * @param	vy		y-component of direction
  * @param	map		worldmap
  */
void raycasting_display(position pos, float vx, float vy, worldmap map)
{
    int j;
    dist_color_and_angle dc;
    float ux;
    float uy;
    //float norme_v = sqrt(vx*vx + vy*vy);
    float hauteur;
    int i1,i2;
    float angle=180*atan(vy/vx)/M_PI,intensity;
    //new_layer();
    for(j=0;j<SCREEN_WIDTH;j++) {
        /*ux = vx / norme_v * DISTANCE_SCREEN_CAMERA - (j - SCREEN_WIDTH/2) * vy / norme_v;
        uy = vy / norme_v * DISTANCE_SCREEN_CAMERA + (j - SCREEN_WIDTH/2) * vx / norme_v;*/
        ux = vx * DISTANCE_SCREEN_CAMERA - (j - SCREEN_WIDTH/2) * vy;
        uy = vy * DISTANCE_SCREEN_CAMERA + (j - SCREEN_WIDTH/2) * vx;
        dc = dda(pos, ux, uy, map);
        //printf("Result dda: %f, %d\n", dc.distance, dc.col);
        hauteur = sqrt(ux*ux + uy*uy) / dc.distance * WALL_SIZE;
        i2 = SCREEN_HEIGHT/2+round(CAMERA_HEIGHT*hauteur/WALL_SIZE);
        i1 = SCREEN_HEIGHT/2-round((WALL_SIZE-CAMERA_HEIGHT)*hauteur/WALL_SIZE);        //printf("args %f, %f, %f, %f\n", pos.x, pos.y, ux, uy);
        //printf("%d, %d, %d, %d\n", j, (int) (SCREEN_HEIGHT/2 - hauteur/2), (int) (SCREEN_HEIGHT/2 + hauteur/2),(int) dc.col);
        intensity = (.6+.4*cos(M_PI*(angle-dc.angle)/180)*cos(M_PI*(angle-dc.angle)/180))*exp(-dc.distance/(WALL_SIZE*MAP_WIDTH));
        //printf("angle = %lf, intensity = %lf\n", angle, intensity);
        draw_seg_v(j, i1, i2,dc.col,intensity);
    }
}
