/* TP Informatique ENSEA 
 * La technique du raycasting
 * 
 * Important :
 * Fonctionne avec SDL version 1
 * Paquet libsdl1.2-dev sous Ubuntu 14.7 */

#include <SDL/SDL.h>
#include <SDL/SDL_image.h>
typedef int colorDisplay;

void pause();
void draw_seg_v(int j, int i1, int i2, colorDisplay couleur, float intensity);
void update_screen();
