#include "affichage.h"
#include "common.h"
#include <SDL/SDL.h>
#include <SDL/SDL_image.h> 

/* Déclaration des variables et fonctions internes à affichage.c */

SDL_Surface *ecran = NULL;
SDL_Surface *lignes[SCREEN_WIDTH] = {NULL};
Uint32 couleurs[6];
Uint32 nuances[SCREEN_WIDTH];
int debut_segment[SCREEN_WIDTH];
int fin_segment[SCREEN_WIDTH];
int couleur_segment[SCREEN_WIDTH];

SDL_Surface *sol;
SDL_Rect solPosition;

/* Déclaration des variables pour l'affichage d'images */
SDL_Surface *mainPouce = NULL;
SDL_Rect positionMainPouce;

int vie = 3;
SDL_Surface *coeur = NULL;
SDL_Rect positionCoeur[3];

	                  
int initialized = 0;

//Initialisation de l'affichage
void init();
void finit();

void init() {
	initialized = 1;
	
	SDL_Init(SDL_INIT_VIDEO);
	
	IMG_Init(IMG_INIT_JPG);
	
	//Position des images à afficher pour l'UI
	positionMainPouce.x = 350;
	positionMainPouce.y = 350;
	
	for(int i=0; i<vie; i++){
		positionCoeur[i].x = i*50;
		positionCoeur[i].y = 0;
	}

	// Changer SDL_FULLSCREEN en SDL_HWSURFACE pour un affichage
	// en mode fenêtre.
	ecran = SDL_SetVideoMode(SCREEN_WIDTH, SCREEN_HEIGHT, 32, SDL_HWSURFACE | SDL_DOUBLEBUF);
	
	
	sol = SDL_CreateRGBSurface(0, SCREEN_WIDTH, SCREEN_HEIGHT, 32, 0, 0, 0, 0);
	solPosition.x = 0;
	solPosition.y = SCREEN_HEIGHT/2;
	
	// Au cas où on est en mode fenêtre, le titre est "Raycasting"
	SDL_WM_SetCaption("Raycasting", NULL);
	
	// Définition des couleurs
	couleurs[0] = SDL_MapRGB(ecran->format, 0, 0, 0);
	couleurs[1] = SDL_MapRGB(ecran->format, 0, 0, 255);
	couleurs[2] = SDL_MapRGB(ecran->format, 255, 0, 0);
	couleurs[3] = SDL_MapRGB(ecran->format, 0, 255, 255);
	couleurs[4] = SDL_MapRGB(ecran->format, 255, 0, 255);
	couleurs[5] = SDL_MapRGB(ecran->format, 255, 100, 255);
	
	int j;
	for(j=0;j<SCREEN_WIDTH;j++) {
		couleur_segment[j] = 0;
	}
	atexit(finit);
}         

void pause() {
	int continuer = 1;
	SDL_Event event;
	
	while (continuer) {
		SDL_WaitEvent(&event);
		switch(event.type) {
			case SDL_QUIT: continuer = 0;
		}
	}
}

void draw_seg_v(int j, int i1, int i2, colorDisplay c, float intensity) {
	Uint32 currentcol=couleurs[c];
	unsigned char *channel=(unsigned char*)(&currentcol);
	//Changement de l'intensité de la couleur
	channel[1] = intensity*channel[1];
	channel[2] = intensity*channel[2];
	channel[3] = intensity*channel[3];
	debut_segment[j] = i1;
	fin_segment[j] = i2;
	nuances[j]=currentcol;
}

void update_screen() {
	int j;
	SDL_Rect position;
	
	if (initialized == 0) init();
	
	// On efface l'écran
	SDL_FillRect(ecran, NULL, SDL_MapRGB(ecran->format, 0,0,0));
	
	//Affichage du sol et du ciel
	SDL_FillRect(ecran, NULL, SDL_MapRGB(ecran->format, 135,206,235));
	SDL_FillRect(sol, NULL, SDL_MapRGB(sol->format, 211,211,211));
	SDL_BlitSurface(sol, NULL, ecran, &solPosition);
	
	for(j=0;j<SCREEN_WIDTH;j++) {
		position.x = j;
		position.y = debut_segment[j];
		
		if (lignes[j] != NULL) SDL_FreeSurface(lignes[j]);
		
		lignes[j] = SDL_CreateRGBSurface(SDL_FULLSCREEN,1,
			(int) (fin_segment[j]-debut_segment[j]),32,0,0,0,0);
		SDL_FillRect(lignes[j],NULL, nuances[j]);		
		SDL_BlitSurface(lignes[j], NULL, ecran, &position);
	}
	
	//Affichage de L'UI
	mainPouce = IMG_Load("pics/main.png");
	SDL_BlitSurface(mainPouce, NULL, ecran, &positionMainPouce);
	
	//Gestion de la vie
	coeur = IMG_Load("pics/coeur.png");
	int i;
	for(i=0;i<vie;i++){
		SDL_BlitSurface(coeur, NULL, ecran, &positionCoeur[i]);
	}
	/*if(vie == 0){
		//afficher un GAME OVER
	}*/

	SDL_Flip(ecran);
}

void finit() {
	int j;

	// désallocation
	for (j=0;j<SCREEN_WIDTH;j++) SDL_FreeSurface(lignes[j]);
	
	SDL_Quit();
}
