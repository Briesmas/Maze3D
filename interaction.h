/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __INTERACTION_H
#define __INTERACTION_H

#ifdef __cplusplus
 extern "C" {
#endif

#include <math.h>
#include <stdio.h>
#include <fmod.h>
#include "common.h"

/** The player shall not get closer to a wall than this distance in block units. */
#define WALL_SECURITY_DISTANCE .05
/** Forward movement speed factor. */
#define SPEED 5
/** Rotational movement speed factor. */
#define ANGLE_INCREMENT 2*M_PI/70.0

int arrow_up(position *, float , float , worldmap );
int arrow_down(position *, float , float, worldmap);
void arrow_right(float *, float * );
void arrow_left(float *, float * );

#ifdef __cplusplus
}
#endif

#endif /* __INTERACTION_H */
