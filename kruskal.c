#include "kruskal.h"

void depart_arrive(int map[tab_SIZE][tab_SIZE]){
	map[tab_SIZE-2][tab_SIZE-2]=5;
}

void affiche_tab(int tab[tab_SIZE][tab_SIZE]){
		int i,j;
		printf("\n");
		for(i=0;i<tab_SIZE;i++){
			for(j=0;j<tab_SIZE;j++){
				printf("%d ", tab[i][j]);
			}
			printf("\n");
		}
}	

void init_tableau(int tab[tab_SIZE][tab_SIZE]){
	int i,j;
	int compteur=1;
	for(i=0;i<tab_SIZE;i++){
		for(j=0;j<tab_SIZE;j++){
			if((i == 0) || (i==(tab_SIZE-1))){
				tab[i][j] = 1;
			}
			else if(((i != 0) && (i!=(tab_SIZE-1))) && ((j == 0) || (j==(tab_SIZE-1)))){
				tab[i][j] = 1;
			}
			else if((i%2 == 0 || j%2 == 0) && i!=0 && i!=tab_SIZE-1 && j!=0 && j!=tab_SIZE-1){
				tab[i][j]=0;
			}
			else{
				tab[i][j] = compteur;
				compteur++;
			}
		}	
	}
}

// tire un nombre aléatoire dans l'interval [a,b[
int rand_a_b(int a, int b){
	return (rand()%(b-a)+a);
}

int label (int map[tab_SIZE][tab_SIZE]){
	int i,j;
	int label1, label2;
	label1=map[1][1];
	
	for(i=1;i<tab_SIZE-1;i++){
		for(j=1;j<tab_SIZE-1;j++){
			if(map[i][j]!=label1){
				label2=map[i][j];
			}
		}
	}
	
	for(i=1;i<tab_SIZE-1;i++){
		for(j=1;j<tab_SIZE-1;j++){
			if(map[i][j]!=label1 && map[i][j]!=label2){
				return 1;
			}
		}
	}
	return 0;	
}

void Kruskal(int tab[tab_SIZE][tab_SIZE]){
	int x_,y_;
	int *x=&x_, *y=&y_;
	int dir_;
	int *dir=&dir_;
	
	while (label(tab)==1){
		pick_x_y(x,y);
		choose_dir(x,y,dir);
		
		if(*dir == 0){
			if(tab[*x][*y] != tab[*x][*y-2]){
				ouverture_mur (tab, *x, *y, *dir);
			}
			else continue;
		}
		if(*dir == 1){
			if(tab[*x][*y] != tab[*x-2][*y]){
				ouverture_mur (tab, *x, *y, *dir);
			}
			else continue;
		}
		if(*dir == 2){
			if(tab[*x][*y] != tab[*x][*y+2]){
				ouverture_mur (tab, *x, *y, *dir);
			}
			else continue;
		}
		if(*dir == 3){
			if(tab[*x][*y] != tab[*x+2][*y]){
				ouverture_mur (tab, *x, *y, *dir);
			}
			else continue;
		}
	}
}

void print_x_y_dir (int x, int y, int dir){
	printf("\n x = %d",x);
	printf("\n y = %d",y);
	printf("\n dir = %d\n",dir);
}

void pick_x_y (int *x, int *y){
	*x = 2;
	*y = 2;
	
	while((*x % 2) == 0){
		*x = rand_a_b(1,tab_SIZE-1);
	}
	while((*y % 2) == 0){
		*y = rand_a_b(1,tab_SIZE-1);
	}
	//printf("x= %d, y= %d \n",*x, *y);
}

void choose_dir (int *x, int *y, int *dir){
	// Coin:
	if (*x == 1 && *y ==1){ //Coin haut gauche
		*dir = rand_a_b(2,4);
	}
	if(*x == 1 && *y == tab_SIZE-2){ //Coiun haut droite
		*dir = rand_a_b(0,2);
		if(*dir == 1){
			*dir = 3;
		}
	}
	if(*x == tab_SIZE-2 && *y == 1){ //Coin bas gauche
		*dir = rand_a_b(1,3);
	}
	if(*x == tab_SIZE-2 && *y == tab_SIZE-2){ //Coin bas droite
		*dir = rand_a_b(0,2);
	}
	
	
	if(*x == 1 && *y != 1 && *y != tab_SIZE-2){ //Ligne 1 sans les coins
		*dir = rand_a_b(0,3);
		if(*dir == 1){
			*dir = 3;
		}
	}
	if(*x == tab_SIZE-2 && *y != 1 && *y != tab_SIZE-2){ // last ligne sans les coins
		*dir = rand_a_b(0,3);
	}
	if(*x != 1 && *x != tab_SIZE-2 && *y == 1){ //ligne gauche sans les coins
		*dir = rand_a_b(1,4);
	}
	if(*x != 1 && *x != tab_SIZE-2 && *y == tab_SIZE-2){ //ligne droite sans les coins
		*dir = rand_a_b(0,3);
		if(*dir == 2){
			*dir = 3;
		}
	}
	if(*x != 1 && *x != tab_SIZE-2 && *y != 1 && *y != tab_SIZE-2){ //Pas sur les cotés
		*dir = rand_a_b(0,4);
	}
}

void ouverture_mur (int tab[tab_SIZE][tab_SIZE], int x, int y, int dir){
	if(dir == 0){ //couverture du mur de gauche
		if(tab[x][y-2]<=tab[x][y]){
			replace(tab,tab[x][y],tab[x][y-2]);
			tab[x][y-1] = tab[x][y-2];
		}
		else{
			replace(tab,tab[x][y-2],tab[x][y]);
			tab[x][y-1] = tab[x][y];
		}
	}
	if(dir == 1){ //ouverture du mur du haut
		if(tab[x-2][y]<=tab[x][y]){
			replace(tab,tab[x][y],tab[x-2][y]);
			tab[x-1][y] = tab[x-2][y];
		}
		else{
			replace(tab,tab[x-2][y],tab[x][y]);
			tab[x-1][y] = tab[x][y];
		}
	}
	if(dir == 2){ //ouverture du mur de droite
		if(tab[x][y+2]<=tab[x][y]){
			replace(tab,tab[x][y],tab[x][y+2]);
			tab[x][y+1] = tab[x][y+2];
		}
		else{
			replace(tab,tab[x][y+2],tab[x][y]);
			tab[x][y+1] = tab[x][y];
		}
	}
	if(dir == 3){ //ouverture du mur du bas
		if(tab[x+2][y]<=tab[x][y]){
			replace(tab,tab[x][y],tab[x+2][y]);
			tab[x+1][y] = tab[x+2][y];
		}
		else{
			replace(tab,tab[x+2][y],tab[x][y]);
			tab[x+1][y] = tab[x][y];
		}
	}
}

void replace (int tab[tab_SIZE][tab_SIZE], int val1, int val2){
	int i,j;
	for(i=1;i<tab_SIZE-1;i++){
		for (j=1;j<tab_SIZE-1;j++){
			if(tab[i][j] == val1){
				tab[i][j]=val2;
			}
		}
	}
}

void replace_1_en_0 (int tab[tab_SIZE][tab_SIZE]){
	int i,j;
	for(i=1;i<tab_SIZE-1;i++){
		for (j=1;j<tab_SIZE-1;j++){
			if(tab[i][j]==0){
				tab[i][j]=2;
			}
			else{
				tab[i][j]=0;
			}
		}
	}
}

void map_crea(worldmap tab1, int tab2[tab_SIZE][tab_SIZE]){
	int i,j;
	for(i=0;i<tab_SIZE;i++){
		for (j=0;j<tab_SIZE;j++){
			tab1[i][j]=tab2[i][j];
		}
	}
}

void affiche_map(worldmap map){
	int i,j;
	for(i=0;i<tab_SIZE;i++){
		for(j=0;j<tab_SIZE;j++){
			printf("%d",map[i][j]);
		}
		printf("\n");
	}
}
