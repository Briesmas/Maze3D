#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include "common.h"

#define tab_SIZE MAP_WIDTH //Doit être obligatoirement impair
#define T 100

void init_tableau(int tab[tab_SIZE][tab_SIZE]);
void affiche_tab(int tab[tab_SIZE][tab_SIZE]);

void Kruskal (int tab[tab_SIZE][tab_SIZE]);
void pick_x_y (int *x, int *y);
void choose_dir (int *x, int *y, int *dir);
void ouverture_mur (int tab[tab_SIZE][tab_SIZE], int x, int y, int dir);
void print_x_y_dir (int x, int y, int dir);
int test_couple (int *x, int *y, int *dir,int map[tab_SIZE][tab_SIZE]);
void replace (int tab[tab_SIZE][tab_SIZE], int val1, int val2);
void replace_1_en_0 (int tab[tab_SIZE][tab_SIZE]);
int rand_a_b(int a, int b);
int label (int map[tab_SIZE][tab_SIZE]);
void depart_arrive(int map[tab_SIZE][tab_SIZE]);
void map_crea(worldmap tab1, int tab2[tab_SIZE][tab_SIZE]);
void affiche_map(worldmap map);
