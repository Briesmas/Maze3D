CC = gcc
FLAGS = -g -Wall -Werror

all : main

main: 	main.o interaction.o affichage.o raycasting.o kruskal.o
		$(CC) -o main interaction.o main.o kruskal.o affichage.o raycasting.o $(FLAGS) -lSDL -lm -lSDL_image -lfmod

affichage.o:	affichage.c affichage.h common.h
				$(CC) -o affichage.o -c affichage.c $(FLAGS) 

interaction.o:	interaction.c interaction.h common.h
				$(CC) -o interaction.o -c interaction.c $(FLAGS)
				
raycasting.o: 	raycasting.c raycasting.h common.h
				$(CC) -o raycasting.o -c raycasting.c $(FLAGS)
				
kruskal.o:		kruskal.c kruskal.h common.h
				$(CC) -o kruskal.o -c kruskal.c $(FLAGS)

main.o:	main.c interaction.h affichage.h raycasting.h common.h
		$(CC) -o main.o -c main.c $(FLAGS)

clean:	
		rm -rf *.o
		rm -rf main
